package hr.fer.oprpp1.hw08.jnotepadpp.local;

public interface ILocalizationProvider {
	/**
	 * Metoda koja dodaje novi {@link ILocalizationListener}
	 * @param listener {@link ILocalizationListener} koji želite dodati
	 */
	void addLocalizationListener(ILocalizationListener listener);
	
	/**
	 * Metoda koja miče {@link ILocalizationListener}
	 * @param listener {@link ILocalizationListener} koji želite maknuti
	 */
	void removeLocalizationListener(ILocalizationListener listener);
	
	/**
	 * Metoda koja vraća prijevod za predani {@code key}
	 * @param key Ključ preko kojeg se dohvaća prijevod
	 * @return Prijevod za predani ključ
	 */
	String getString(String key);
	
	/**
	 * Vraća trenutni jezik
	 * @return trenutni jezik
	 */
	String getLanguage();
}
