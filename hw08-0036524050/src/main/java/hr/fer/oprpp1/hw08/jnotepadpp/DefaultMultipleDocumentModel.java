package hr.fer.oprpp1.hw08.jnotepadpp;

import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import hr.fer.oprpp1.hw08.jnotepadpp.local.FormLocalizationProvider;
import hr.fer.oprpp1.hw08.jnotepadpp.local.ILocalizationListener;
import hr.fer.oprpp1.hw08.jnotepadpp.models.MultipleDocumentListener;
import hr.fer.oprpp1.hw08.jnotepadpp.models.MultipleDocumentModel;
import hr.fer.oprpp1.hw08.jnotepadpp.models.SingleDocumentListener;
import hr.fer.oprpp1.hw08.jnotepadpp.models.SingleDocumentModel;

public class DefaultMultipleDocumentModel extends JTabbedPane implements MultipleDocumentModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<MultipleDocumentListener> listeners;
	
	private List<SingleDocumentModel> documents;
	private SingleDocumentModel current;
	private FormLocalizationProvider provider;
	
	/**
	 * Javni konstruktor koji dodaje {@link ILocalizationListener}-a na {@code provider} kako bi se mogli mijenjati imena tabova
	 * @param provider {@link FormLocalizationProvider} na kojeg se želite pretplatiti za promjenu jezika
	 */
	public DefaultMultipleDocumentModel(FormLocalizationProvider provider) {
		this.provider = provider;
		provider.addLocalizationListener(new ILocalizationListener() {
			
			@Override
			public void localizationChanged() {
				for (SingleDocumentModel doc : documents)
					if (doc.getFilePath() == null)
						DefaultMultipleDocumentModel.this.setTitleAt(getIndexOfDocument(doc), provider.getString("unnamed"));
				
			}
		});
		listeners = new ArrayList<>();
		documents = new ArrayList<>();
		this.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				SingleDocumentModel old = current;
				if (DefaultMultipleDocumentModel.this.getSelectedIndex() >= 0)
					current = documents.get(DefaultMultipleDocumentModel.this.getSelectedIndex());
				else
					current = null;
				for (MultipleDocumentListener l : listeners)
					l.currentDocumentChanged(old, current);
			}
		});
	}

	@Override
	public Iterator<SingleDocumentModel> iterator() {
		return documents.iterator();
	}

	@Override
	public JComponent getVisualComponent() {
		return this;
	}

	@Override
	public SingleDocumentModel createNewDocument() {
		SingleDocumentModel newDoc = new DefaultSingleDocumentModel(null, "");
		documents.add(newDoc);
		this.addTab("unnamed", new JScrollPane(newDoc.getTextComponent()));
		
		for(MultipleDocumentListener l : listeners) {
			l.documentAdded(newDoc);
		}
		this.setSelectedIndex(getIndexOfDocument(newDoc));
		addListener(newDoc);
		this.setIconAt(getIndexOfDocument(newDoc), getIcon("green"));
		return newDoc;
	}

	@Override
	public SingleDocumentModel getCurrentDocument() {
		return current;
	}

	@Override
	public SingleDocumentModel loadDocument(Path path) {
		if(!Files.isReadable(path)) {
			JOptionPane.showMessageDialog(
					this, 
					path.toFile().getAbsolutePath() + " " + provider.getString("loadF"), 
					provider.getString("error"), 
					JOptionPane.ERROR_MESSAGE);
			return null;
		}
		for(SingleDocumentModel doc : documents) {
			if (path.equals(doc.getFilePath())) {
				this.setSelectedIndex(getIndexOfDocument(doc));
				return null;
			}
		}
		byte[] okteti;
		try {
			okteti = Files.readAllBytes(path);
			String tekst = new String(okteti, StandardCharsets.UTF_8);
			SingleDocumentModel newDoc = new DefaultSingleDocumentModel(path, tekst);
			documents.add(newDoc);
			this.addTab(path.toFile().getName(), new JScrollPane(newDoc.getTextComponent()));
			this.setSelectedIndex(getIndexOfDocument(newDoc));
			this.setIconAt(getIndexOfDocument(newDoc), getIcon("green"));
			for(MultipleDocumentListener l : listeners) {
				l.documentAdded(newDoc);
			}
			addListener(newDoc);
			return newDoc;
		} catch(Exception ex) {
			JOptionPane.showMessageDialog(
					this, 
					"Pogreška prilikom čitanja datoteke " + path.toFile().getAbsolutePath() + ".", 
					provider.getString("error"), 
					JOptionPane.ERROR_MESSAGE);
			return null;
		}
		
	}

	@Override
	public void saveDocument(SingleDocumentModel model, Path newPath) {
		Path path;
		if (!model.isModified())
			return;
		if (newPath != null) {
			for(SingleDocumentModel doc : documents) {
				if (newPath.equals(doc.getFilePath()))
					throw new IllegalArgumentException();
			}
			path = newPath;
			model.setFilePath(path);	
		} else if(model.getFilePath() == null) {
			throw new NullPointerException();
		} else 
			path = model.getFilePath();
		
		byte[] podatci = model.getTextComponent().getText().getBytes(StandardCharsets.UTF_8);
		try {
			Files.write(path, podatci);	
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(
					this, 
					provider.getString("saveFailed") + " " + path.toFile().getAbsolutePath(), 
					provider.getString("loadF"), 
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		model.setModified(false);
		JOptionPane.showMessageDialog(
				DefaultMultipleDocumentModel.this, 
				provider.getString("saveS"),
				provider.getString("information"), 
				JOptionPane.INFORMATION_MESSAGE);
		
		return;
	}


	@Override
	public void closeDocument(SingleDocumentModel model) {
		int index = getIndexOfDocument(model);
		documents.remove(index);
		this.removeTabAt(index);
		for(MultipleDocumentListener l : listeners) {
			l.documentRemoved(model);
		}
	}

	@Override
	public void addMultipleDocumentListener(MultipleDocumentListener l) {
		listeners.add(l);
	}

	@Override
	public void removeMultipleDocumentListener(MultipleDocumentListener l) {
		listeners.remove(l);
	}

	@Override
	public int getNumberOfDocuments() {
		return documents.size();
	}

	@Override
	public SingleDocumentModel getDocument(int index) {
		return documents.get(index);
	}

	@Override
	public SingleDocumentModel findForPath(Path path) {
		for (SingleDocumentModel l : documents) {
			if (l.getFilePath() == path) {
				return l;
			}
		}
		return null;
	}

	@Override
	public int getIndexOfDocument(SingleDocumentModel doc) {
		for (int i = 0; i < documents.size(); i++) {
			if (doc == documents.get(i))
				return i;
		}
		return -1;
	}
	
	/**
	 * Pomoćna metoda koja dodaje {@link SingleDocumentListener} na predani {@link SingleDocumentModel} prilikom otvaranja novog dokumenta i učitavanja postojećeg
	 * 
	 * <br>{@link SingleDocumentListener} se koristi za promjenu ikone i promjenu imena taba
	 * @param model {@link SingleDocumentModel} na kojeg želite dodati {@link SingleDocumentListener}
	 */
	private void addListener(SingleDocumentModel model) {
		model.addSingleDocumentListener(new SingleDocumentListener() {
			
			@Override
			public void documentModifyStatusUpdated(SingleDocumentModel model) {
				int index = getIndexOfDocument(model);
				if (model.isModified()) {
					DefaultMultipleDocumentModel.this.setIconAt(index, getIcon("red"));
				}
				else {
					DefaultMultipleDocumentModel.this.setIconAt(index, getIcon("green"));
				}
					
			}
			
			@Override
			public void documentFilePathUpdated(SingleDocumentModel model) {
				setTitleAt(getIndexOfDocument(model), model.getFilePath().toFile().getName());
				return;	
			}
		});
	}
	
	/**
	 * Metoda koja za predani tekst (green ili red) dohvaća odgovarajuću ikonu
	 * @param text "green" ili "red"
	 * @return {@link ImageIcon}
	 */
	private ImageIcon getIcon(String text) {
		String path = "icons/" + text + ".png";
		InputStream is = this.getClass().getResourceAsStream(path);
		if(is==null) 
			throw new IllegalArgumentException();
		byte[] bytes;
		try {
			bytes = is.readAllBytes();
			is.close();
			ImageIcon imageIcon = new ImageIcon(bytes); // load the image to a imageIcon
			Image image = imageIcon.getImage(); // transform it 
			Image newimg = image.getScaledInstance(15, 15,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
			return new ImageIcon(newimg);  // transform it back
			
		} catch (IOException e) {
			System.out.println("Nesto poslo po krivu");
			return null;
		}
		
	}
}
