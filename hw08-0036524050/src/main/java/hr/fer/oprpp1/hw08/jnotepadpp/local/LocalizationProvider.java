package hr.fer.oprpp1.hw08.jnotepadpp.local;

import java.util.Locale;
import java.util.ResourceBundle;

public class LocalizationProvider extends AbstractLocalizationProvider {
	private String language;
	private ResourceBundle bundle;
	private static final LocalizationProvider instance = new LocalizationProvider();
	
	private LocalizationProvider() {
		language = "en";
		bundle = ResourceBundle.getBundle("hr.fer.oprpp1.hw08.jnotepadpp.local.prijevodi", Locale.forLanguageTag(language));
	}
	

	@Override
	public String getString(String key) {
		return bundle.getString(key);
	}
	
	
	/**
	 * Metoda koja postavlja trenutni jezik u {@code language} i pritom objaveštava se prijavljene {@link ILocalizationListener}-e da je došlo do promjene
	 * @param language Jezik koji želite postaviti kao trenutni
	 */
	public void setLanguage(String language) {
		this.language = language;
		bundle = ResourceBundle.getBundle("hr.fer.oprpp1.hw08.jnotepadpp.local.prijevodi", Locale.forLanguageTag(language));
		this.fire();
	}
	
	
	/**
	 * Dohvaća instancu {@link ILocalizationProvider}-a 
	 * @return instanca {@link ILocalizationProvider}-a
	 */
	public static LocalizationProvider getInstance() {
		return instance;
	}

	@Override
	public String getLanguage() {
		return language;
	}

}
