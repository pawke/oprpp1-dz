package hr.fer.oprpp1.hw08.jnotepadpp;

import java.awt.Component;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

import hr.fer.oprpp1.hw08.jnotepadpp.models.SingleDocumentListener;
import hr.fer.oprpp1.hw08.jnotepadpp.models.SingleDocumentModel;

public class DefaultSingleDocumentModel extends Component implements SingleDocumentModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<SingleDocumentListener> listeners;
	private JTextArea textArea;
	private boolean modified;
	private Path path;
	private Document document;
	
	
	
	public DefaultSingleDocumentModel(Path path, String content) {
		this.path = path;
		listeners = new ArrayList<>();
		textArea = new JTextArea(content);
		DocumentListener listener = new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				modified = true;
				fire();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				modified = true;
				fire();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				modified = true;
				fire();				
			}
		};
		document = textArea.getDocument();
		document.addDocumentListener(listener);
		modified = false;
	}

	@Override
	public JTextArea getTextComponent() {
		return textArea;
	}

	@Override
	public Path getFilePath() {
		return path;
	}

	@Override
	public void setFilePath(Path path) {
		this.path = path;
		for(SingleDocumentListener l : listeners) {
			l.documentFilePathUpdated((SingleDocumentModel) this);
		}
	}

	@Override
	public boolean isModified() {
		return this.modified;
	}

	@Override
	public void setModified(boolean modified) {
		this.modified = modified;
		fire();
		
	}

	@Override
	public void addSingleDocumentListener(SingleDocumentListener l) {
		listeners.add(l);
	}

	@Override
	public void removeSingleDocumentListener(SingleDocumentListener l) {
		listeners.remove(l);
	}
	
	private void fire() {
		for(SingleDocumentListener l : listeners) {
			l.documentModifyStatusUpdated((SingleDocumentModel) this);
		}
	}

}
