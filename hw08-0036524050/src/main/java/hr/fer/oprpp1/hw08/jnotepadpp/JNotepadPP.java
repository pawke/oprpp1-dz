package hr.fer.oprpp1.hw08.jnotepadpp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.nio.file.Path;
import java.text.Collator;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.function.BiPredicate;
import java.util.function.UnaryOperator;

import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import hr.fer.oprpp1.hw08.jnotepadpp.local.FormLocalizationProvider;
import hr.fer.oprpp1.hw08.jnotepadpp.local.ILocalizationListener;
import hr.fer.oprpp1.hw08.jnotepadpp.local.LocalizableAction;
import hr.fer.oprpp1.hw08.jnotepadpp.local.LocalizationProvider;
import hr.fer.oprpp1.hw08.jnotepadpp.models.MultipleDocumentListener;
import hr.fer.oprpp1.hw08.jnotepadpp.models.MultipleDocumentModel;
import hr.fer.oprpp1.hw08.jnotepadpp.models.SingleDocumentListener;
import hr.fer.oprpp1.hw08.jnotepadpp.models.SingleDocumentModel;


public class JNotepadPP extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private MultipleDocumentModel model;
	private JLabel lengthLabel, coursorLabel, clockLabel;
	private JMenu changeCase;
	private FormLocalizationProvider flp = new FormLocalizationProvider(LocalizationProvider.getInstance(), this);
	
	/**
	 * Javni konstruktor koji dodaje {@link ILocalizationListener} za promjenu imena prozora i promjene teksta u labeli za prikazivanje duljine teksta prilikom promjene teksta
	 * <br> Dodaje {@link MultipleDocumentListener} za promjenu imena prozora u slučaju promjene trenutnog taba
	 */
	public JNotepadPP() {
		flp.addLocalizationListener(new ILocalizationListener() {
			
			@Override
			public void localizationChanged() {
				JNotepadPP.this.setTitle("(" + flp.getString("unnamed") + ") - JNotepadPP");
				if (model.getCurrentDocument() != null)
					lengthLabel.setText(flp.getString("length") + ": " + model.getCurrentDocument().getTextComponent().getText().length());
					
			}
		});
		model = new DefaultMultipleDocumentModel(flp);
		model.addMultipleDocumentListener(new MultipleDocumentListener() {
			
			@Override
			public void documentRemoved(SingleDocumentModel model) {
				
			}
			
			@Override
			public void documentAdded(SingleDocumentModel model) {
				
			}
			
			@Override
			public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
				if (currentModel != null) {	
					String name;
					if (currentModel.getFilePath() != null)
						name = currentModel.getFilePath().toAbsolutePath().toString();
					else
						name = "(" + flp.getString("unnamed") + ")";
					name = name + " - JNotepad++";
					JNotepadPP.this.setTitle(name);
					updateCaret(model.getCurrentDocument().getTextComponent());
					updateLength(currentModel);
				}
			}
		});
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		setLocation(0, 0);
		setSize(600, 600);
		setTitle("(" + flp.getString("unnamed") + ") - JNotepadPP");
		
		initGUI();
	}

	/**
	 * Pomoćna metoda koja dodaje sve elemente {@link JNotepadPP} u {@link JFrame}
	 */
	private void initGUI() {
		WindowListener listener = new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				Iterator<SingleDocumentModel> iterator = model.iterator();
				SingleDocumentModel document;
				while(iterator.hasNext()) {
					document = iterator.next();
					if (document.isModified()) {
						String name;
						if(document.getFilePath() != null)
							name = document.getFilePath().toFile().getName();
						else
							name = "unnamed";
						int a = JOptionPane.showConfirmDialog(JNotepadPP.this, name + " " + flp.getString("notSaved"));
						if (a == JOptionPane.CANCEL_OPTION)
							return;
						else if(a == JOptionPane.YES_OPTION) {
							Path path = document.getFilePath();
							if (path == null)
								path = getSaveDocumentPath();
							model.saveDocument(document, path);
						}
					}
				}
				dispose();
			}
			
		};
		JPanel panel = new JPanel(new BorderLayout());
		panel.add(model.getVisualComponent(), BorderLayout.CENTER);
		JPanel labels = new JPanel(new GridLayout(1, 3));
		lengthLabel = new JLabel();
		lengthLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
		coursorLabel = new JLabel();
		coursorLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
		clockLabel = new JLabel();
		clockLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		clockLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY, 1));
		labels.add(lengthLabel);
		labels.add(coursorLabel);
		labels.add(clockLabel);
		panel.add(labels, BorderLayout.SOUTH);
		
		this.addWindowListener(listener);
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(panel);
		
		createActions();
		createMenus();
		createToolbars();
		
		ActionListener alistener = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Date date = new Date();
				DateFormat timeformat = new SimpleDateFormat("HH:mm:ss");
				DateFormat dateformat = new SimpleDateFormat("YYYY/MM/dd");
				clockLabel.setText(dateformat.format(date) + " " + timeformat.format(date));
			}
		};
		Timer timer = new Timer(1000, alistener);
		timer.setRepeats(true);
		timer.setCoalesce(true);
		timer.setInitialDelay(0);
		timer.start();
	}
	
	private Action loadDocumentAction = new LocalizableAction("loadName", "loadDes", flp) {
		
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fc = new JFileChooser();
			fc.setDialogTitle("Open file");
			if(fc.showOpenDialog(JNotepadPP.this)!=JFileChooser.APPROVE_OPTION) {
				return;
			}
			Path path = fc.getSelectedFile().toPath();
			SingleDocumentModel document = model.loadDocument(path);
			if (document != null) {
				addSingleDocumentListener(document);
				addCaretListener(document.getTextComponent());
			}
		}
	};
	
	private Action createNewDocumentAction = new LocalizableAction("createName", "createDes", flp) {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			SingleDocumentModel document = model.createNewDocument();
			addSingleDocumentListener(document);
			addCaretListener(document.getTextComponent());
		}
	};
	
	private Action saveDocumentAction = new LocalizableAction("saveName", "saveDes", flp) {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				Path path = null;
				if (model.getCurrentDocument().getFilePath() == null)
					path = getSaveDocumentPath();
				model.saveDocument(model.getCurrentDocument(), path);
			} catch (NullPointerException ex) {
			} catch (IllegalArgumentException ex) {
				JOptionPane.showMessageDialog(
					JNotepadPP.this, 
					flp.getString("saveF"), 
					flp.getString("warning"), 
					JOptionPane.WARNING_MESSAGE);
			}
		}
	};
	
	private Action saveAsDocumentAction = new LocalizableAction("saveAsName", "saveAsDes", flp) {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				Path path = getSaveDocumentPath();
				model.saveDocument(model.getCurrentDocument(), path);
				} catch (NullPointerException ex) {
				} catch (IllegalArgumentException ex) {
					JOptionPane.showMessageDialog(
							JNotepadPP.this, 
							flp.getString("saveF"), 
							flp.getString("warning"), 
							JOptionPane.WARNING_MESSAGE);
				}
		}
	};
	
	private Action closeDocumentAction = new LocalizableAction("closeName", "closeDes", flp) {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if (model.getCurrentDocument() != null)
				model.closeDocument(model.getCurrentDocument());
			
		}
	};
	
	private Action getStatisticsAction = new LocalizableAction("infoName", "infoDes", flp) {
		
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			SingleDocumentModel document = model.getCurrentDocument();
			if (document != null) {
				String text = document.getTextComponent().getText();
				int a = 0, b = 0;
				for (char c : text.toCharArray()) {
					if(c == ' ' || c == '\t' || c == '\r')
						a++;
					else if (c == '\n')
						b++;
				}
				JOptionPane.showMessageDialog(
						JNotepadPP.this, 
						flp.getString("info1") + text.length() + " " + flp.getString("info2") + (text.length() - a - b) + " " + flp.getString("info3") + (b + 1) + " " + flp.getString("info4"), 
						flp.getString("statistics"), 
						JOptionPane.INFORMATION_MESSAGE);
			}
		}
	};
	
	private Action toUpperCaseAction = new LocalizableAction("upperName", "upperDes", flp) {
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			changeText(StringOperators.TO_UPPER_CASE);
		}
	};
	
	private Action toLowerCaseAction = new LocalizableAction("lowerName", "lowerDes", flp) {
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			changeText(StringOperators.TO_LOWER_CASE);
		}
	};
	
	private Action toggleCaseAction = new LocalizableAction("toggleName", "toggleDes", flp) {
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			changeText(StringOperators.TOGGLE_CASE);
		}
	};
	
	private Action sortAscendingAction = new LocalizableAction("sortAscName", "sortAscDes", flp) {
		
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			sortiraj(StringOperators.SORT_ASC);
		}		
	};
	
	private Action sortDescendingAction = new LocalizableAction("sortDscName", "sortDscDes", flp) {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			sortiraj(StringOperators.SORT_DESC);
		}		
	};
	
	private Action uniqueAction = new LocalizableAction("uniqueName", "uniqueDes", flp) {
		private static final long serialVersionUID = 1L;
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JTextArea tArea = model.getCurrentDocument().getTextComponent();
			Document doc = tArea.getDocument();
			if (tArea.getCaret().getDot() != tArea.getCaret().getMark()) {
				int pocetak = Math.min(tArea.getCaret().getDot(),tArea.getCaret().getMark());
				int kraj = Math.max(tArea.getCaret().getDot(),tArea.getCaret().getMark());
				try {
					int mini = tArea.getLineStartOffset(tArea.getLineOfOffset(pocetak));
					int maxi = tArea.getLineEndOffset(tArea.getLineOfOffset(mini));
					String pocetnaLinija = doc.getText(mini, maxi - mini);
					mini = maxi;
					while (mini < kraj) {
						boolean fixed = false;
						maxi = tArea.getLineEndOffset(tArea.getLineOfOffset(mini));
						String linija = doc.getText(mini, maxi - mini);
						if (!linija.contains("\n")) {
							linija += "\n";
							fixed = true;
						}
						if (pocetnaLinija.equals(linija)) {
							if (!fixed)
								doc.remove(mini, linija.length());
							else
								doc.remove(mini, linija.length() - 1);
							kraj -= maxi - mini;
							continue;
						} 
						mini = maxi;
						
						
					}
				} catch (BadLocationException e1) {
					
					e1.printStackTrace();
				}
			}
		}
	};
	
	
	/**
	 * Pomoćna metoda koja akcijama postavlja keyStroke i MnemonicKeyEvent
	 */
	private void createActions() {
		loadDocumentAction.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control shift L")); 
		loadDocumentAction.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_L); 
		
		createNewDocumentAction.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control O")); 
		createNewDocumentAction.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_O); 

		saveDocumentAction.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control S")); 
		saveDocumentAction.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_S); 

		saveAsDocumentAction.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control shift S")); 
		saveAsDocumentAction.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_V); 
		
		closeDocumentAction.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control Z")); 
		closeDocumentAction.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_C); 

		getStatisticsAction.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control I")); 
		getStatisticsAction.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_I); 
		
		toUpperCaseAction.putValue( 
				Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke("control U"));
		toUpperCaseAction.putValue(Action.MNEMONIC_KEY
				, KeyEvent.VK_U);
		
		toLowerCaseAction.putValue( 
				Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke("control L"));
		toLowerCaseAction.putValue(Action.MNEMONIC_KEY
				, KeyEvent.VK_L);
		
		toggleCaseAction.putValue( 
				Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke("control T"));
		toggleCaseAction.putValue(Action.MNEMONIC_KEY
				, KeyEvent.VK_T);
		
		uniqueAction.putValue( 
				Action.ACCELERATOR_KEY,
				KeyStroke.getKeyStroke("control shift U"));
		uniqueAction.putValue(Action.MNEMONIC_KEY
				, KeyEvent.VK_N);
	}
	
	/**
	 * Pomoćna metoda koja kreira {@link JMenuBar}, {@link JMenuItem}-e i dodaje {@link ILocalizationListener} za promjenu jezika svih {@link JMenuItem}-a
	 */
	private void createMenus() {
		JMenuBar menuBar = new JMenuBar();
		
		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);

		fileMenu.add(new JMenuItem(loadDocumentAction));
		fileMenu.add(new JMenuItem(saveDocumentAction));
		fileMenu.add(new JMenuItem(saveAsDocumentAction));
		fileMenu.add(new JMenuItem(createNewDocumentAction));
		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(closeDocumentAction));
		fileMenu.add(new JMenuItem(getStatisticsAction));
		
		JMenu tools = new JMenu(flp.getString("tools"));
		
		JMenu sorts = new JMenu(flp.getString("sort"));
		sorts.add(sortAscendingAction);
		sorts.add(sortDescendingAction);
		tools.add(sorts);
		
		changeCase = new JMenu(flp.getString("changeCase"));
		changeCase.add(new JMenuItem(toUpperCaseAction));
		changeCase.add(new JMenuItem(toLowerCaseAction));
		changeCase.add(new JMenuItem(toggleCaseAction));
		
		
		tools.add(changeCase);
		tools.add(new JMenuItem(uniqueAction));
		
		menuBar.add(tools);
		JMenuItem en = new JMenuItem("en");
		JMenuItem hr = new JMenuItem("hr");
		JMenuItem de = new JMenuItem("de");
		
		ActionListener listener = a -> {
			JMenuItem b = (JMenuItem)a.getSource();
			LocalizationProvider.getInstance().setLanguage(b.getText());
		};
		en.addActionListener(listener);
		de.addActionListener(listener);
		hr.addActionListener(listener);
		
		JMenu menu = new JMenu("Languages");

		menu.add(en);
		menu.add(hr);
		menu.add(de);
		menuBar.add(menu);
		
		this.setJMenuBar(menuBar);
		flp.addLocalizationListener(new ILocalizationListener() {
			
			@Override
			public void localizationChanged() {
				tools.setText(flp.getString("tools"));
				changeCase.setText(flp.getString("changeCase"));
				menu.setText(flp.getString("languages"));
				fileMenu.setText(flp.getString("fileMenu"));
			}
		});
		
	}
	
	/**
	 * Pomoćna metoda koja dodaje {@link JToolBar}, dodaje {@link ILocalizationListener} za promjenu jezika {@link JToolBar}-a
	 */
	private void createToolbars() {
		JToolBar toolBar = new JToolBar(flp.getString("toolBar"));
		toolBar.setFloatable(true);
		
		toolBar.add(new JButton(saveAsDocumentAction));
		toolBar.add(new JButton(saveDocumentAction));
		toolBar.add(new JButton(createNewDocumentAction));
		toolBar.add(new JButton(loadDocumentAction));
		toolBar.addSeparator();
		toolBar.add(new JButton(closeDocumentAction));
		toolBar.add(new JButton(getStatisticsAction));
		flp.addLocalizationListener(new ILocalizationListener() {
			
			@Override
			public void localizationChanged() {
				toolBar.setName(flp.getString("toolBar"));;
			}
		});
		
		this.getContentPane().add(toolBar, BorderLayout.PAGE_START);
	}
	
	
	/**
	 * Metoda koja dodaje novi {@link CaretListener} na predani {@link JTextArea}
	 * @param textArea {@link JTextArea} na koji želite dodati {@link CaretListener}
	 */
	private void addCaretListener(JTextArea textArea) {
		textArea.addCaretListener(new CaretListener() {
			
			@Override
			public void caretUpdate(CaretEvent e) {
				updateCaret(textArea);
			}
		});
	}
	
	
	/**
	 * Pomoćna metoda koja updatea labelu za informacije o kursoru, te omogućava ili onemogućava {@code changeCase} {@link JMenuItem} ovisno je li označen tekst ili ne
	 * @param textArea {@link JTextArea} iz kojeg izvlačimo informacije o kursoru
	 */
	private void updateCaret(JTextArea textArea) {
		String update;
		int position = textArea.getCaretPosition();
		char[] array = textArea.getText().toCharArray();
		int br = 1;
		int zadnjiN = 0;
		for (int i = 0; i < array.length; i++) {
			if (i >= position)
				break;
			if (array[i] == '\n') {
				br++;
				zadnjiN = i;
			}
		}
		int len = Math.abs(textArea.getCaret().getDot() - textArea.getCaret().getMark());
		int col;
		if (br == 1)
			col = position + 1;
		else
			col = position - zadnjiN;
		update = "Ln : " + br + "  Col : " + col + "  Sel : " + len;
		coursorLabel.setText(update);
		if (len > 0)
			changeCase.setEnabled(true);
		else
			changeCase.setEnabled(false);
	}
	
	/**
	 * Pomoćna metoda koja osvježava labelu za prikaz duljine dokumenta
	 * @param document
	 */
	private void updateLength(SingleDocumentModel document) {
		int len = document.getTextComponent().getText().length();
		lengthLabel.setText(flp.getString("length") + ": " + len);
	}
	
	/**
	 * Pomoćna metoda koja dodaje {@link SingleDocumentListener} na predani {@link SingleDocumentModel} kako bi se osvježavale labele i ime prozora
	 * @param document
	 */
	private void addSingleDocumentListener(SingleDocumentModel document) {
		document.addSingleDocumentListener(new SingleDocumentListener() {
			
			
			@Override
			public void documentModifyStatusUpdated(SingleDocumentModel model) {
				updateLength(model);
			}
			
			@Override
			public void documentFilePathUpdated(SingleDocumentModel model) {
				String name = model.getFilePath().toString() + " - JNotepad++";
				JNotepadPP.this.setTitle(name);
				
			}
		});
	}
	
	/**
	 * Pomoćna metoda koja radi interakciju sa userom i vraća odabranu Putanju
	 * 
	 * @return odabrana putanja
	 */
	private Path getSaveDocumentPath() {
		JFileChooser jfc = new JFileChooser();
		jfc.setDialogTitle("Save document");
		if(jfc.showSaveDialog(JNotepadPP.this)!=JFileChooser.APPROVE_OPTION) {
			JOptionPane.showMessageDialog(
					JNotepadPP.this, 
					"Ništa nije snimljeno.", 
					flp.getString("warning"), 
					JOptionPane.WARNING_MESSAGE);
			throw new NullPointerException();
		}
		return jfc.getSelectedFile().toPath();
	
	}
	
	private void sortiraj(BiPredicate<String, String> sort) {
		JTextArea tArea = model.getCurrentDocument().getTextComponent();
		Document doc = tArea.getDocument();
		if (tArea.getCaret().getDot() != tArea.getCaret().getMark()) {
			int pocetak = Math.min(tArea.getCaret().getDot(),tArea.getCaret().getMark());
			int kraj = Math.max(tArea.getCaret().getDot(),tArea.getCaret().getMark());	
			int len = 0;
			try {
				List<String> lista = new ArrayList<>();
				int mini = tArea.getLineStartOffset(tArea.getLineOfOffset(pocetak));
				int maxi = tArea.getLineEndOffset(tArea.getLineOfOffset(mini));
				String pocetnaLinija = doc.getText(mini, maxi - mini);
				mini = maxi;
				lista.add(pocetnaLinija);
				len += pocetnaLinija.length();
				boolean end = true;
				while (mini < kraj) {
					maxi = tArea.getLineEndOffset(tArea.getLineOfOffset(mini));
					lista.add(doc.getText(mini, maxi - mini));
					end = doc.getText(mini, maxi - mini).endsWith("\n");
					len += maxi - mini;
					mini = maxi;
				}
				if (lista.size() <= 1)
					return;
				for (int i = 0; i < lista.size(); i++) {
					for (int j = i; j < lista.size(); j++) {
						if (sort.test(lista.get(i), lista.get(j))){
							String pom = lista.get(i);
							lista.set(i, lista.get(j));
							lista.set(j, pom);
						}
					}
				}
				int prvi = tArea.getLineStartOffset(tArea.getLineOfOffset(pocetak));
				doc.remove(prvi, len);
				int br = 1;
				for (String l : lista) {
					if (!l.endsWith("\n"))
						l += "\n";
					if (br == lista.size() && end == false && l.endsWith("\n"))
						l = l.substring(0, l.length() - 1);
					doc.insertString(prvi, l, null);
					prvi += l.length();
					br ++;
				}
			} catch (BadLocationException e1) {
				
				e1.printStackTrace();
			}
			
		}
	}

	private void changeText(UnaryOperator<String> operator) {
		JTextArea tArea = model.getCurrentDocument().getTextComponent();
		Document doc = tArea.getDocument();
		int len = Math.abs(tArea.getCaret().getDot() - tArea.getCaret().getMark());
		int offset = Math.min(tArea.getCaret().getDot(),tArea.getCaret().getMark());
		
		try {
			String text = doc.getText(offset, len);
			text = operator.apply(text);
			doc.remove(offset, len);
			doc.insertString(offset, text, null);
		} catch(BadLocationException ex) {
			ex.printStackTrace();
		}
	}

	public static void main(String[] args) {
			SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				new JNotepadPP().setVisible(true);
			}
		});

	}

}
