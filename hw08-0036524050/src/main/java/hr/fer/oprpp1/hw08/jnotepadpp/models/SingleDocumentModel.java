package hr.fer.oprpp1.hw08.jnotepadpp.models;

import java.nio.file.Path;

import javax.swing.JTextArea;

public interface SingleDocumentModel {
	
	/**
	 * Metoda koja vraća {@link JTextArea} trenutnog {@link SingleDocumentModel}
	 * @return {@link JTextArea} treutnog {@link SingleDocumentModel}
	 */
	JTextArea getTextComponent();
	
	/**
	 * Metoda koja vraća putanju trenutnog {@link SingleDocumentModel}
	 * @return putanja treutnog {@link SingleDocumentModel}
	 */
	Path getFilePath();
	
	/**
	 * Metoda koja postavlja putanju
	 * @param path Putanja koju želite postaviti trenutnom {@link SingleDocumentModel}
	 */
	void setFilePath(Path path);
	
	/**
	 * Metoda koja govori je li {@link SingleDocumentModel} mijenjan
	 * @return {@code true} ako je {@link SingleDocumentModel} mijenjan, inače false
	 */
	boolean isModified();
	
	/**
	 * Metoda koja postavlja je li {@link SingleDocumentModel} mijenjan na {@code modified}
	 * @param modified Boolean na koji želite postaviti je li mijenjan {@link SingleDocumentModel}
	 */
	void setModified(boolean modified);
	
	/**
	 * Metoda koja dodaje novi {@link SingleDocumentListener} u trenutni {@link SingleDocumentModel}
	 * @param l {@link SingleDocumentListener} koji želite dodati
	 */
	void addSingleDocumentListener(SingleDocumentListener l);
	
	/**
	 * Metoda koja miče {@link SingleDocumentListener} iz trenutnog {@link SingleDocumentModel}
	 * @param l {@link SingleDocumentListener} koji želite maknuti
	 */
	void removeSingleDocumentListener(SingleDocumentListener l);
}
