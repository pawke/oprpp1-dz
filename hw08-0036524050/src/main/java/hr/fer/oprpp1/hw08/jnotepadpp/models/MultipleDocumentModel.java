package hr.fer.oprpp1.hw08.jnotepadpp.models;

import java.nio.file.Path;

import javax.swing.JComponent;

public interface MultipleDocumentModel extends Iterable<SingleDocumentModel> {
	/**
	 * Metoda koja vraća {@link JComponent} trenutnog {@link MultipleDocumentModel}
	 * @return {@link JComponent} treuntog {@link MultipleDocumentModel}
	 */
	JComponent getVisualComponent();
	
	/**
	 * Metoda koja dodaje novi {@link SingleDocumentModel} u trenutni {@link MultipleDocumentModel}
	 * @return {@link SingleDocumentModel} koji je dodan u trenutni {@link MultipleDocumentModel}
	 */
	SingleDocumentModel createNewDocument();
	
	/**
	 * Metoda koja dohvaća trenutni aktivan {@link SingleDocumentModel}
	 * @return treutno aktivan {@link SingleDocumentModel}
	 */
	SingleDocumentModel getCurrentDocument();
	
	/**
	 * Metoda koja dodaje novi {@link SingleDocumentModel} u trenutni {@link MultipleDocumentModel} sa predanog patha
	 * @param path Putanja datoteke koju želite dodati
	 * @return {@link SingleDocumentModel} koji je dodan
	 */
	SingleDocumentModel loadDocument(Path path);
	
	/**
	 * Metoda koja sprema {@link SingleDocumentModel} na disc
	 * @param model {@link SingleDocumentModel} koji želite spremiti
	 * @param newPath Putanja na koju želite spremiti {@code model} ili null ako želite koristiti putanju iz {@code model}
	 */
	void saveDocument(SingleDocumentModel model, Path newPath);
	
	/**
	 * Metoda koja zatvara trenutni {@link SingleDocumentModel}
	 * @param model {@link SingleDocumentModel} koji želite zatvoriti
	 */
	void closeDocument(SingleDocumentModel model);
	
	/**
	 * Metoda koja dodaje novi {@link MultipleDocumentListener}
	 * @param l {@link MultipleDocumentListener} koji želite dodati
	 */
	void addMultipleDocumentListener(MultipleDocumentListener l);
	
	/**
	 * Metoda koja miče {@link MultipleDocumentListener}
	 * @param l {@link MultipleDocumentListener} koji želite maknuti
	 */
	void removeMultipleDocumentListener(MultipleDocumentListener l);
	
	/**
	 * Metoda koja vraća broj {@link SingleDocumentModel} 
	 * @return broj {@link SingleDocumentModel} 
	 */
	int getNumberOfDocuments();
	
	/**
	 * Vraća {@link SingleDocumentModel} na predanom indexu
	 * @param index Index {@link SingleDocumentModel} koji želite dohvatiti
	 * @return {@link SingleDocumentModel} na indexu {@code index}
	 */
	SingleDocumentModel getDocument(int index);
	
	/**
	 * Vraća {@link SingleDocumentModel} sa predanim {@code path}
	 * @param path Putanja za koju želite dohvatiti {@link SingleDocumentModel}
	 * @return {@link SingleDocumentModel} sa putanjom {@code path}
	 */
	SingleDocumentModel findForPath(Path path); //null, if no such model exists
	
	/**
	 * Vraća index predanog {@link SingleDocumentModel}
	 * @param doc {@link SingleDocumentModel} za koji želite dohvatiti index
	 * @return index predanog {@link SingleDocumentModel}
	 */
	int getIndexOfDocument(SingleDocumentModel doc); //-1 if not present
}

