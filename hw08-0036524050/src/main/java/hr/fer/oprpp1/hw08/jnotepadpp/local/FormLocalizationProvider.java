package hr.fer.oprpp1.hw08.jnotepadpp.local;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;

public class FormLocalizationProvider extends LocalizationProviderBridge {
	
	
	/**
	 * Javni konstruktor koji stvara novu instancu {@link FormLocalizationProvider}-a i postavlja {@link WindowListener} na trenutni prozor
	 * @param provider {@link ILocalizationListener} od kojeg želite dobivati informaciju ako je promijenjen jezik
	 * @param frame {@link JFrame} u koji dodajete objekt
	 */
	public FormLocalizationProvider(ILocalizationProvider provider, JFrame frame) {
		super(provider);
		frame.addWindowListener(new WindowAdapter() {

			@Override
			public void windowOpened(WindowEvent e) {
				conect();
			}

			@Override
			public void windowClosed(WindowEvent e) {
				disonect();
			}
			
		});
	}

}
