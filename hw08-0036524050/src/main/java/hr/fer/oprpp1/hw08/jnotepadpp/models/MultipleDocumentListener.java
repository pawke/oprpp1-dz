package hr.fer.oprpp1.hw08.jnotepadpp.models;


public interface MultipleDocumentListener {
	/**
	 * Metoda koja se izvršava kada se promijeni tab u MultipleDocumentModel-u
	 * @param previousModel prijašnji {@link SingleDocumentModel}
	 * @param currentModel trenutni {@link SingleDocumentModel}
	 */
	void currentDocumentChanged(SingleDocumentModel previousModel,
			SingleDocumentModel currentModel);
	/**
	 * Metoda koja se izvršava kada se doda novi {@link SingleDocumentModel} u trenutni {@link MultipleDocumentModel}
	 * @param model {@link SingleDocumentModel} koji se dodao
	 */
	void documentAdded(SingleDocumentModel model);
	
	/**
	 * Metoda koja se izvršava kada se makne neki {@link SingleDocumentModel} iz trenutnog {@link MultipleDocumentModel}
	 * @param model {@link SingleDocumentModel} koji se maknuo
	 */
	void documentRemoved(SingleDocumentModel model);
}
