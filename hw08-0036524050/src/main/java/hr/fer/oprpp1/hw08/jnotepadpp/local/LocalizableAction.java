package hr.fer.oprpp1.hw08.jnotepadpp.local;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

public abstract class LocalizableAction extends AbstractAction {
	

	private static final long serialVersionUID = 1L;
	private String name;
	private String description;
	private ILocalizationProvider provider;
	
	/**
	 * Javni konstruktor koji stvara novu instancu {@link LocalizableAction} i pretplaćuje se na {@code provider} za promjenu jezika
	 * @param name Ključ po kojem se dohvaća prijevod za ime akcije
	 * @param description Ključ po kojem se dohvaća prijevod za opis akcije
	 * @param provider {@link ILocalizationProvider} na kojeg se želite pretplatiti za promjenu jezika
	 */
	public LocalizableAction(String name, String description, ILocalizationProvider provider) {
		this.name = name;
		this.description = description;
		this.provider = provider;
		
		this.putValue(Action.NAME, provider.getString(name));
		this.putValue(Action.SHORT_DESCRIPTION, provider.getString(description));
		
		provider.addLocalizationListener(new ILocalizationListener() {
			
			@Override
			public void localizationChanged() {
				LocalizableAction.this.putValue(Action.NAME, provider.getString(name));
				LocalizableAction.this.putValue(Action.SHORT_DESCRIPTION, provider.getString(description));				
			}
		});
	}


}
