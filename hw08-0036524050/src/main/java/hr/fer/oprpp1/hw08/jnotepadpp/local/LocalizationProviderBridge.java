package hr.fer.oprpp1.hw08.jnotepadpp.local;

public class LocalizationProviderBridge extends AbstractLocalizationProvider {
	private boolean conected;
	private ILocalizationProvider provider;
	private String language;
	
	private ILocalizationListener listener = new ILocalizationListener() {
		
		@Override
		public void localizationChanged() {
			language = provider.getLanguage();
			LocalizationProviderBridge.this.fire();
			
		}
	};
	
	public LocalizationProviderBridge(ILocalizationProvider provider) {
		this.provider = provider;
		this.language = "en";
	}
	
	/**
	 * metoda koja se pretplaćuje na {@link ILocalizationProvider}-a
	 */
	public void conect() {
		if (!conected) {
			conected = true;
			provider.addLocalizationListener(listener);
			if (!provider.getLanguage().equals(language)) {
				language = provider.getLanguage();
				fire();
			}
		}
	}
	
	/**
	 * Metoda koja se odjavljuje sa {@link ILocalizationProvider}-a
	 */
	public void disonect() {
		provider.removeLocalizationListener(listener);
	}

	@Override
	public String getString(String key) {
		return provider.getString(key);
	}

	@Override
	public String getLanguage() {
		return language;
	}

}
