package hr.fer.oprpp1.hw08.jnotepadpp.local;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractLocalizationProvider implements  ILocalizationProvider{
	List<ILocalizationListener> listeners;

	@Override
	public void addLocalizationListener(ILocalizationListener listener) {
		listeners.add(listener);
		
	}

	@Override
	public void removeLocalizationListener(ILocalizationListener listener) {
		listeners.remove(listener);
		
	}

	public AbstractLocalizationProvider() {
		this.listeners = new ArrayList<>();
	}
	
	/**
	 * Metoda koja obavještava sve {@link ILocalizationListener}-e da je došlo do promjene jezika
	 */
	public void fire() {
		for (ILocalizationListener l : listeners)
			l.localizationChanged();
	}

}
