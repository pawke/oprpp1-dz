package hr.fer.oprpp1.hw08.jnotepadpp;

import java.text.Collator;
import java.util.Locale;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.UnaryOperator;

/**
 * Klasa koja sadrži operatore za rad sa stringovima
 * @author ivorp
 *
 */
public class StringOperators {
	public static final UnaryOperator<String> TO_UPPER_CASE = s -> {
		return s.toUpperCase();
	};
	
	public static final UnaryOperator<String> TO_LOWER_CASE = s -> {
		return s.toLowerCase();
	};
	
	public static final UnaryOperator<String> TOGGLE_CASE = s -> {
		char[] znakovi = s.toCharArray();
		for(int i = 0; i < znakovi.length; i++) {
			char c = znakovi[i];
			if(Character.isLowerCase(c)) {
				znakovi[i] = Character.toUpperCase(c);
			} else if(Character.isUpperCase(c)) {
				znakovi[i] = Character.toLowerCase(c);
			}
		}
		return new String(znakovi);
	};
	
	public static final BiPredicate<String, String> SORT_ASC = (a, b) -> {
		Locale hrLocale = new Locale("hr");
		Collator hrCollator = Collator.getInstance(hrLocale);
		return hrCollator.compare(a, b) > 0;
	};
	
	public static final BiPredicate<String, String> SORT_DESC = (a, b) -> {
		Locale hrLocale = new Locale("hr");
		Collator hrCollator = Collator.getInstance(hrLocale);
		return hrCollator.compare(a, b) < 0;
	};
}
