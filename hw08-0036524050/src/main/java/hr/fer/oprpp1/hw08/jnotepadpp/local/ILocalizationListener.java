package hr.fer.oprpp1.hw08.jnotepadpp.local;

public interface ILocalizationListener {
	
	/**
	 * Metoda koja se izvršava kada je promijenjen jezik
	 */
	void localizationChanged();
}
