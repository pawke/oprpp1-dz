package hr.fer.oprpp1.hw08.jnotepadpp.models;

public interface SingleDocumentListener {
	/**
	 * Metoda koja se izvršava nakon što je promijenjen {@link SingleDocumentModel}
	 * @param model {@link SingleDocumentModel} koji je promijenjen
	 */
	void documentModifyStatusUpdated(SingleDocumentModel model);
	
	/**
	 * Metoda koja se izvršava nakon što je promijenjane putanja nekom {@link SingleDocumentModel}
	 * @param model {@link SingleDocumentModel} kojem je promijenjena putanja
	 */
	void documentFilePathUpdated(SingleDocumentModel model);
}
