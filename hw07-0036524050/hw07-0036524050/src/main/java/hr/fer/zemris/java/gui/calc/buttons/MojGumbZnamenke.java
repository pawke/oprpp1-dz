package hr.fer.zemris.java.gui.calc.buttons;

import java.awt.event.ActionListener;

import javax.swing.JButton;

import hr.fer.zemris.java.gui.calc.CalcModel;

public class MojGumbZnamenke extends JButton{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CalcModel model;
	
	ActionListener action = a -> {
		try {
			JButton b = (JButton)a.getSource();
			model.insertDigit(Integer.valueOf(b.getText()));
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	};
	
	/**
	 * Konstruktor koji stvara novi gumb koji na pritisak stavlja zapisanu znamenku u model
	 * @param text Znamenka koju želite prikazivati
	 * @param model Trenutni model koji se koristi
	 */
	public MojGumbZnamenke(String text, CalcModel model) {
		super(text);
		this.addActionListener(action);
		this.setFont(this.getFont().deriveFont(30f));
		this.model = model;
	}
	
}
