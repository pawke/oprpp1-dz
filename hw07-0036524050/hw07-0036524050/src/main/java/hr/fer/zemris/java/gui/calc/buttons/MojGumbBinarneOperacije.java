package hr.fer.zemris.java.gui.calc.buttons;

import java.awt.event.ActionListener;
import java.util.function.DoubleBinaryOperator;

import javax.swing.JButton;

import hr.fer.zemris.java.gui.calc.CalcModel;

/**
 * Klasa koja predstavlja gumb binarnih operacija
 * @author ivorp
 *
 */
public class MojGumbBinarneOperacije extends JButton{
	private static final long serialVersionUID = 1L;
	
	private CalcModel model;
	private DoubleBinaryOperator operator;
	
	ActionListener action = a -> {
		if (model.getPendingBinaryOperation() == null) {
			model.setPendingBinaryOperation(operator);
		} else {
			double value = model.getPendingBinaryOperation().applyAsDouble(model.getActiveOperand(), model.getValue());
			model.setValue(value);
			model.setActiveOperand(value);
			model.freezeValue(Double.valueOf(value).toString());
			model.setPendingBinaryOperation(operator);
		}
	};
	
	/**
	 * Konstruktor koji stvara novu instancu razreda i dodaje tom gumbu ActionListener
	 * @param text Tekst koji se preikazuje na gumbu
	 * @param model Model u koji želite zapisivati vrijednosti
	 * @param operator Operacija koju želite obaviti kada se pritisne gumb
	 */
	public MojGumbBinarneOperacije(String text, CalcModel model, DoubleBinaryOperator operator) {
		super(text);
		this.addActionListener(action);
		this.model = model;
		this.operator = operator;
	}
}
