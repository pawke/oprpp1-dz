package hr.fer.zemris.java.gui.calc;

import java.util.ArrayList;
import java.util.List;
import java.util.function.DoubleBinaryOperator;

public class CalcModelImpl implements CalcModel {
	private boolean editable;
	private boolean positive;
	private String currentNumber;
	private double value;
	private String frozenValue;
	private double activeOperand;
	private DoubleBinaryOperator pendingBinaryOperation;
	private List<CalcValueListener> listeners;
	private boolean added = false;
	
	public CalcModelImpl() {
		editable = true;
		positive = true;
		currentNumber = "0";
		value = 0;
		frozenValue = null;
		activeOperand = 0;
		pendingBinaryOperation = null;
		listeners = new ArrayList<>();
	}

	@Override
	public void addCalcValueListener(CalcValueListener l) {
		listeners.add(l);

	}

	@Override
	public void removeCalcValueListener(CalcValueListener l) {
		listeners.remove(l);
	}

	@Override
	public double getValue() {
		return value;
	}

	@Override
	public void setValue(double value) {
		this.value = value;
		currentNumber = Double.valueOf(value).toString();
		frozenValue = null;
		update();
		editable = false;
	}

	@Override
	public boolean isEditable() {
		return editable;
	}

	@Override
	public void clear() {
		currentNumber = "0";
		value = 0;
		update();
		added = false;
		editable = true;
	}

	@Override
	public void clearAll() {
		pendingBinaryOperation = null;
		activeOperand = 0;
		frozenValue = null;
		this.clear();
		update();
	}

	@Override
	public void swapSign() throws CalculatorInputException {
		if (!editable) 
			throw new CalculatorInputException("Ne možete mijenjati ovaj broj");
		positive = !positive;
		if (positive)
			currentNumber = currentNumber.substring(1);
		else
			currentNumber = "-" + currentNumber;
		value = Double.valueOf(currentNumber);
		update();
	}

	@Override
	public void insertDecimalPoint() throws CalculatorInputException {
		if (!editable)
			throw new CalculatorInputException("Ne možete mijenjati ovaj broj");
		if (currentNumber.contains(".") || !added)
			throw new CalculatorInputException("Broj već sadrži decimalnu točku");
		currentNumber += ".";
		update();
	}

	@Override
	public void insertDigit(int digit) throws CalculatorInputException, IllegalArgumentException {
		if (!editable)
			throw new CalculatorInputException("Ne možete mijanjati ovaj broj");
		if(digit < 0 || digit > 9)
			throw new IllegalArgumentException("Broj mora biti između 0 i 9");
		if (Math.abs(value * 10.0) >= Double.MAX_VALUE)
			throw new CalculatorInputException("Broj bi prekoračio maksimalnu dozvoljenu vrijednost");
		if (currentNumber.equals("0"))
			currentNumber = Integer.valueOf(digit).toString();
		else
			currentNumber += Integer.valueOf(digit).toString();
		frozenValue = null;
		
		value = Double.valueOf(currentNumber);
		update();
		added = true;
	}

	@Override
	public boolean isActiveOperandSet() {
		if (activeOperand != 0)
			return true;
		return false;
	}

	@Override
	public double getActiveOperand() throws IllegalStateException {
		if (isActiveOperandSet())
			return activeOperand;
		throw new IllegalStateException("Ne postoji aktivni operand");
	}

	@Override
	public void setActiveOperand(double activeOperand) {
		this.activeOperand = activeOperand;
	}

	@Override
	public void clearActiveOperand() {
		this.activeOperand = 0;

	}

	@Override
	public DoubleBinaryOperator getPendingBinaryOperation() {
		return pendingBinaryOperation;
	}

	@Override
	public void setPendingBinaryOperation(DoubleBinaryOperator op) {
		editable = true;
		if (op == null)
			pendingBinaryOperation = null;
		else {
			this.pendingBinaryOperation = op;
			if (activeOperand == 0)
				activeOperand = value;
			if (frozenValue == null)
				frozenValue = currentNumber;
			clear();
		}
	}
	
	@Override
	public String toString() {
		String ret;
		if (!hasFrozenValue()) 
			ret = currentNumber;
		else 
			ret = frozenValue;
//		if (ret.endsWith(".0"))
//			return ret.substring(0, ret.length() - 2);
		return ret;
	}

	@Override
	public void freezeValue(String value) {
		frozenValue = value;
		update();
	}

	@Override
	public boolean hasFrozenValue() {
		return frozenValue != null;
	}
	
	private void update() {
		for (CalcValueListener listener : listeners) {
			listener.valueChanged(this);
		}
	}
}
