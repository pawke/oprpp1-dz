package hr.fer.zemris.java.gui.calc.buttons;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.function.DoubleUnaryOperator;

import javax.swing.JButton;

import hr.fer.zemris.java.gui.calc.CalcModel;

/**
 * Gumb za unarne operacije
 * @author ivorp
 *
 */
public class MojGumbUnarneOperacije extends JButton{
	private static final long serialVersionUID = 1L;
	private CalcModel model;
	String primarno, sekundarno, trenutno;
	DoubleUnaryOperator prvi, drugi;
	
	ActionListener action = a -> {
		DoubleUnaryOperator operator;
		if (trenutno.equals(primarno))
			operator = prvi;
		else
			operator = drugi;
		double value = operator.applyAsDouble(model.getValue());
		model.setValue(value);
		model.freezeValue(Double.valueOf(value).toString());
		model.setActiveOperand(value);
	};
	
	
	/**
	 * Konstruktor koji prima model koji se koristi, te 2 alternativna teksta za prikaz na gumbu, kao i 2 alternativne operacije.
	 * Konstruktor stvara novi gumb, prikazani tekst je {@code primarno}, a veličina se prilagođava duljem od dvaju Stringova
	 * @param primarno Tekst koji želite da se primarno prikazuje
	 * @param sekundarno Tekst koji želite da se prikazuje alternativno
	 * @param model Trenutni model koji se koristi
	 * @param prvi Operacija koja će se primarno izvoditi
	 * @param drugi Operacija koja će se izvoditi ako je na gumbu prikazan tekst {@code sekundarno} 
	 */
	public MojGumbUnarneOperacije(String primarno, String sekundarno, CalcModel model, DoubleUnaryOperator prvi, DoubleUnaryOperator drugi) {
		super(primarno.length() > sekundarno.length() ? primarno : sekundarno);
		Dimension size = this.getPreferredSize();
		this.primarno = primarno;
		this.sekundarno = sekundarno;
		this.model = model;
		this.trenutno = primarno;
		this.prvi = prvi;
		this.drugi = drugi;
		this.addActionListener(action);
		this.setText(primarno);
		this.setPreferredSize(size);
	}
	
	public void promijeni() {
		if (trenutno.equals(primarno))
			trenutno = sekundarno;
		else
			trenutno = primarno;
		this.setText(trenutno);
	}
}
