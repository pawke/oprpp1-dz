package hr.fer.zemris.java.gui.calc;

import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleUnaryOperator;

/**
 * Klasa koja sadrži sve operatore potrebne za kalkulator
 * @author ivorp
 *
 */
public class Operators {

	public static final DoubleBinaryOperator ADD = (a, b) -> {
		return a + b;
	};
	
	public static final DoubleBinaryOperator SUB = (a, b) -> {
		return a - b;
	};
	
	public static final DoubleBinaryOperator MUL = (a, b) -> {
		return a * b;
	};
	
	public static final DoubleBinaryOperator DIV = (a, b) -> {
		return a / b;
	};
	
	public static final DoubleBinaryOperator POW = (a, b) -> {
		return Math.pow(a, b);
	};
	
	public static final DoubleBinaryOperator ROOT = (a, b) -> {
		return Math.pow(a, 1 / b);
	};
	
	public static final DoubleUnaryOperator SIN = a -> {
		return Math.sin(a);
	};
	
	public static final DoubleUnaryOperator COS = a -> {
		return Math.cos(a);
	};
	
	public static final DoubleUnaryOperator TAN = a -> {
		return Math.tan(a);
	};
	
	public static final DoubleUnaryOperator CTG = a -> {
		return 1.0 / Math.tan(a);
	};
	
	public static final DoubleUnaryOperator ASIN = a -> {
		return Math.asin(a);
	};
	
	public static final DoubleUnaryOperator ACOS = a -> {
		return Math.acos(a);
	};
	
	public static final DoubleUnaryOperator ATAN = a -> {
		return Math.atan(a);
	};
	
	public static final DoubleUnaryOperator ACTG = a -> {
		return Math.atan(1.0 / a);
	};
	
	public static final DoubleUnaryOperator LOG = a -> {
		return Math.log10(a);
	};
	
	public static final DoubleUnaryOperator ALOG = a -> {
		return Math.pow(10.0, a);
	};
	
	public static final DoubleUnaryOperator LN = a -> {
		return Math.log(a);
	};
	
	public static final DoubleUnaryOperator ALN = a -> {
		return Math.pow(Math.E, a);
	};
}
