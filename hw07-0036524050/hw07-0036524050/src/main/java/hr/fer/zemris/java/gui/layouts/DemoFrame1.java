package hr.fer.zemris.java.gui.layouts;

import java.awt.Color;
import java.awt.Container;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import hr.fer.zemris.java.gui.calc.CalcModel;
import hr.fer.zemris.java.gui.calc.CalcModelImpl;
import hr.fer.zemris.java.gui.calc.CalcValueListener;
import hr.fer.zemris.java.gui.calc.Operators;
import hr.fer.zemris.java.gui.calc.buttons.MojGumbBinarneOperacije;
import hr.fer.zemris.java.gui.calc.buttons.MojGumbPotenciranje;
import hr.fer.zemris.java.gui.calc.buttons.MojGumbUnarneOperacije;
import hr.fer.zemris.java.gui.calc.buttons.MojGumbZnamenke;



public class DemoFrame1 extends JFrame {
public DemoFrame1() {
	setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	setSize(500, 500);
	initGUI();

}
private void initGUI() {
Container cp = getContentPane();
cp.setLayout(new CalcLayout(3));
cp.add(l("tekst 1"), new RCPosition(1,1));
cp.add(l("tekst 2"), new RCPosition(2,3));
cp.add(l("tekst stvarno najdulji"), new RCPosition(2,7));
cp.add(l("tekst kraći"), new RCPosition(4,2));
cp.add(l("tekst srednji"), new RCPosition(4,5));
cp.add(l("tekst"), new RCPosition(4,7));
}
private JLabel l(String text) {
JLabel l = new JLabel(text);
l.setBackground(Color.YELLOW);
l.setOpaque(true);
return l;
}
public static void main(String[] args) {
SwingUtilities.invokeLater(()->{
new DemoFrame1().setVisible(true);
});
}
}
