package hr.fer.zemris.java.gui.charts;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.List;

import javax.swing.JComponent;

/**
 * Klasa koja služi za prikaz grafa u GUI-u
 * @author ivorp
 *
 */
public class BarChartComponent extends JComponent{
	
	private BarChart chart;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Konstruktor koji stvara novu instancu razreda {@link BarChartComponent}
	 * @param chart Graf koji želite staviti u komponentu
	 */
	public BarChartComponent(BarChart chart) {
		this.chart = chart;
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		int x = 0;
		int y = 0;
		int stringHeight = g2d.getFontMetrics().getHeight();
		x += 20 + stringHeight;
		y += 5 + 2 * stringHeight + 10;
		int maxi = 0;
		for (XYValue value : chart.getList()) {
			int duljina = g2d.getFontMetrics().stringWidth(Integer.valueOf(value.getY()).toString());
			if ( duljina > maxi)
				maxi = duljina;
		}
		
		int yStart = this.getHeight() - y + (stringHeight / 4);
		int yMax = chart.getyMin() + chart.getyInc() * (int) Math.ceil((chart.getyMax() - chart.getyMin()) * 1.0 / chart.getyInc());
		
		int yInc = (this.getHeight() - y - 20) / ((yMax - chart.getyMin()) / chart.getyInc());
		if (yInc % 2 != 0) {
			yInc ++;
		}
		crtajBrojeveY(x, yStart, maxi, yInc, g2d, yMax);
		x += maxi + 10;
		yStart = this.getHeight() - y;
		crtajCrteY(x, yStart, yInc, g2d, yMax);
		x += 5;
		crtajKoordSustav(x, yStart, this.getWidth() - 20, 0, g2d);
		
		int xInc = (this.getWidth() - 30 - x) / chart.getList().size();
		crtajX(x, yStart, xInc, g2d, chart.getList());
		crtajStupce(x + 2, yStart, xInc, yInc / 2, g2d, chart.getList());
		
		g2d.setColor(Color.BLACK);
		
		g2d.drawString(chart.getDesX(), (this.getWidth() / 2)  - (g2d.getFontMetrics().stringWidth(chart.getDesX()) / 2), this.getHeight() - 5);
		AffineTransform at = new AffineTransform();
		at.rotate(-Math.PI / 2);
		g2d.setTransform(at);
		//zamijenjene visina i sirina
		g2d.drawString(chart.getDesY(), - (this.getHeight() / 2 + (g2d.getFontMetrics().stringWidth(chart.getDesY()) / 2)), 0 + stringHeight);
		at.rotate(Math.PI / 2);
		g2d.setTransform(at);
		
		
	}
	
	/**
	 * Metoda koja služi za crtanje stupaca
	 * @param x početni x
	 * @param y početni y
	 * @param xInc širina jednog stupca
	 * @param yInc inkrement po y osi
	 * @param g2d {@link Graphics2D} koji služi za crtanje
	 * @param list lista XYValue-a
	 */
	private void crtajStupce(int x, int y, int xInc, int yInc, Graphics2D g2d, List<XYValue> list) {
		for (int z = 0; z < chart.getList().size(); z++) {
			g2d.setColor(Color.RED);
			g2d.fillRect(x, y - (list.get(z).getY() * yInc) , xInc - 1, (list.get(z).getY() * yInc));
			x += xInc;
		}
	}

	/**
	 * Metoda koja označava točke na x osi i zapisuje vrijednosti
	 * @param x početni x
	 * @param y početni y
	 * @param xInc širina stupca
	 * @param g2d {@link Graphics2D} koji služi za crtanje
	 * @param list lista XYValue-a
	 */
	private void crtajX(int x, int y, int xInc, Graphics2D g2d, List<XYValue> list) {
		for (int z = 0; z < chart.getList().size(); z++) {
			g2d.drawLine(x, y + 5, x, y);
			String br = Integer.valueOf(list.get(z).getX()).toString();
			g2d.drawString(br, x + (xInc / 2 - g2d.getFontMetrics().stringWidth(br) / 2), y + g2d.getFontMetrics().getHeight());
			x += xInc;
		}
		g2d.drawLine(x, y + 5, x, y);
		
	}

	/**
	 * Metoda koja označava točke na y osi
	 * @param x početni x
	 * @param y početni y
	 * @param yInc inkrement po y osi
	 * @param g2d {@link Graphics2D} koji služi za crtanje
	 * @param yMax maksimalni y do kojeg se prikazuje
	 */
	private void crtajCrteY(int x, int y, int yInc, Graphics2D g2d, int yMax) {
		for (int z = chart.getyMin(); z <= yMax; z+= chart.getyInc()) {
			g2d.drawLine(x, y, x + 5, y);
			y -= yInc;
		}
	}

	/**
	 * Metoda koja crta koordinatni sustav (bez brojeva na osima)
	 * @param xStart početni x
	 * @param yStart početni y
	 * @param xEnd krajnji x
	 * @param yEnd krajnji y
	 * @param g2d {@link Graphics2D} koji služi za crtanje
	 */
	private void crtajKoordSustav(int xStart, int yStart, int xEnd, int yEnd, Graphics2D g2d) {
		g2d.drawLine(xStart, yStart, xEnd, yStart);
		g2d.drawLine(xEnd - 5, yStart - 5, xEnd, yStart);
		g2d.drawLine(xEnd - 5, yStart + 5, xEnd, yStart);
		g2d.drawLine(xStart, yStart, xStart, yEnd);
		g2d.drawLine(xStart - 5, yEnd + 5, xStart, yEnd);
		g2d.drawLine(xStart + 5, yEnd + 5, xStart, yEnd);
	}
	
	/**
	 * Metoda koja zapisuje brojeve na y os
	 * @param x početni x
	 * @param y početni y
	 * @param maxi maksimalna duljina Stringa koji će se prikazivati
	 * @param yInc inkrement po y osi
	 * @param g2d {@link Graphics2D} koji služi za crtanje
	 * @param yMax maksimalni y koji se prikazuje
	 */
	private void crtajBrojeveY(int x, int y, int maxi, int yInc, Graphics2D g2d, int yMax) {
		for (int z = chart.getyMin(); z <= yMax; z+= chart.getyInc()) {
			g2d.drawString(Integer.valueOf(z).toString(), x + (maxi - g2d.getFontMetrics().stringWidth(Integer.valueOf(z).toString())), y);
			y -= yInc;
		}
	}
	
	

}
