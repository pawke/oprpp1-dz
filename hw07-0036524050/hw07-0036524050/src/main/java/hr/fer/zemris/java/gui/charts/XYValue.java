package hr.fer.zemris.java.gui.charts;

/**
 * klasa koja sprema 2 int vrijednosti, x i y
 * @author ivorp
 *
 */
public class XYValue {
	private int x;
	private int y;
	
	/**
	 * Javni konstruktor koji stvara novu instancu razreda XYValue
	 * @param x X vrijednost
	 * @param y Y vrijednost
	 */
	public XYValue(int x, int y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Metoda koja dohvaća trenutni x
	 * @return trenutni x
	 */
	public int getX() {
		return x;
	}
	
	/**
	 * Metoda koja dohvaća trenutni y
	 * @return trenutni y
	 */
	public int getY() {
		return y;
	}
	
	
}
