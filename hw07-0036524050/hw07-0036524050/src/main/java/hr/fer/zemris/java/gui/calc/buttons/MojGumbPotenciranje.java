package hr.fer.zemris.java.gui.calc.buttons;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.function.DoubleBinaryOperator;

import javax.swing.JButton;

import hr.fer.zemris.java.gui.calc.CalcModel;

/**
 * gumb za potenciranje
 * @author ivorp
 *
 */
public class MojGumbPotenciranje extends JButton{
	private static final long serialVersionUID = 1L;
	private CalcModel model;
	String primarno, sekundarno, trenutno;
	DoubleBinaryOperator prvi, drugi;
	
	ActionListener action = a -> {
		DoubleBinaryOperator operator;
		if (trenutno.equals(primarno))
			operator = prvi;
		else
			operator = drugi;
		if (model.getPendingBinaryOperation() == null) {
			model.setPendingBinaryOperation(operator);
		} else {
			double value = model.getPendingBinaryOperation().applyAsDouble(model.getActiveOperand(), model.getValue());
			model.setActiveOperand(value);
			model.freezeValue(Double.valueOf(value).toString());
			model.setPendingBinaryOperation(operator);
			
		}
	};
	
	/**
	 * Konstruktor koji prima model koji se koristi, te 2 alternativna teksta za prikaz na gumbu, kao i 2 alternativne operacije
	 * Konstruktor stvara novi gumb, prikazani tekst je {@code primarno}, a veličina se prilagođava duljem od dvaju Stringova
	 * @param primarno Tekst koji želite da se primarno prikazuje
	 * @param sekundarno Tekst koji želite da se prikazuje alternativno
	 * @param model Trenutni model koji se koristi
	 * @param prvi Operacija koja će se primarno izvoditi
	 * @param drugi Operacija koja će se izvoditi ako je na gumbu prikazan tekst {@code sekundarno} 
	 */
	public MojGumbPotenciranje(String primarno, String sekundarno, CalcModel model, DoubleBinaryOperator prvi, DoubleBinaryOperator drugi) {
		super(primarno.length() > sekundarno.length() ? primarno : sekundarno);
		Dimension size = this.getPreferredSize();
		this.primarno = primarno;
		this.sekundarno = sekundarno;
		this.model = model;
		this.trenutno = primarno;
		this.prvi = prvi;
		this.drugi = drugi;
		this.addActionListener(action);
		this.setText(primarno);
		this.setPreferredSize(size);
	}
	
	public void promijeni() {
		if (trenutno.equals(primarno))
			trenutno = sekundarno;
		else
			trenutno = primarno;
		this.setText(trenutno);
	}
}
