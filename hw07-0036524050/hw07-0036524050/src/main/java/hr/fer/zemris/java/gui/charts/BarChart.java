package hr.fer.zemris.java.gui.charts;

import java.util.List;

/**
 * Klasa koja predstavlja jedan dijagram
 * @author ivorp
 *
 */
public class BarChart {
	private List<XYValue> list;
	private String desX;
	private String desY;
	private int yMin;
	private int yMax;
	private int yInc;
	
	/** 
	 * Vraća listu XYValue-a
	 * @return lista XYValue-a
	 */
	public List<XYValue> getList() {
		return list;
	}

	/**
	 * Vraća naziv X osi
	 * @return naziv x osi
	 */
	public String getDesX() {
		return desX;
	}

	/**
	 * Vraća naziv Y osi
	 * @return naziv Y osi
	 */
	public String getDesY() {
		return desY;
	}

	/**
	 * Vraća minimalni y
	 * @return minimalni y
	 */
	public int getyMin() {
		return yMin;
	}

	/**
	 * Vraća maksimalni y
	 * @return maksimalni y
	 */
	public int getyMax() {
		return yMax;
	}

	/**
	 * Vraća inkrement po y osi
	 * @return inkrement po y osi
	 */
	public int getyInc() {
		return yInc;
	}

	/**
	 * Konstruktor koji stvara novu instancu razreda BarChart
	 * @param list Lista XYValue koji prestavljaju točke za prikaz u grafu
	 * @param desX Naziv x osi
	 * @param desY Naziv y osi
	 * @param yMin Minimalni y koji želite da se prikazuje
	 * @param yMax Maksimalni y koji želite da se prikazuje
	 * @param yInc Inkrement po y osi
	 * @throws IllegalArgumentException  ako je maksimum manji od minimuma; ako je minimum manji od nule;
	 * ako je bilo koja vrijednost y predana u listi XYValuea manja od minimalnog y 
	 * 
	 */
	public BarChart(List<XYValue> list, String desX, String desY, int yMin, int yMax, int yInc) {
		if(!(yMax > yMin))
			throw new IllegalArgumentException("Maksimum mora biti veći od minimuma");
		if (yMin < 0)
			throw new IllegalArgumentException("Minimum ne smije biti negativan");
		for (XYValue value : list) {
			if (value.getX() < yMin)
				throw new IllegalArgumentException("Svaki y mora biti veći od predanog minumuma");
		}
		this.list = list;
		this.desX = desX;
		this.desY = desY;
		this.yMin = yMin;
		this.yMax = yMax;
		this.yInc = yInc;
	}
	
	
}
