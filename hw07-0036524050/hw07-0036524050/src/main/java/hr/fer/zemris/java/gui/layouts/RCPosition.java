package hr.fer.zemris.java.gui.layouts;


/**
 * Klasa koja predstavlja poziciju unutar CalcLayouta
 * @author ivorp
 *
 */
public class RCPosition {
	private int red;
	private int stupac;
	
	/**
	 * Konstruktor koji stvara novu instancu klase RCPosition
	 * @param red Red u kojem se nalazi željena komponenta
	 * @param stupac Stupac u kojem se nalazi željena komponenta
	 */
	public RCPosition(int red, int stupac) {
		this.red = red;
		this.stupac = stupac;
	}
	
	/**
	 * Dohvaća trenutni red
	 * @return trenutni red
	 */
	public int getRed() {
		return red;
	}

	/**
	 * dohvaća trenutni stupac
	 * @return trenutni stupac
	 */
	public int getStupac() {
		return stupac;
	}
	
	/**
	 * parsira string na RCPosition
	 * @param ulaz String koji predstavlja redak i stupac za RCPosition
	 * @return novi RCPosition
	 */
	public static RCPosition parse(String ulaz) {
		String[] parametri = ulaz.split(",");
		if (parametri.length != 2)
			throw new IllegalArgumentException();
		
		return new RCPosition(Integer.parseInt(parametri[0]), Integer.parseInt(parametri[0]));
	}
}
