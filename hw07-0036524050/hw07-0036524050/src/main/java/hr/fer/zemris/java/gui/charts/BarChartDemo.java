package hr.fer.zemris.java.gui.charts;

import java.awt.BorderLayout;
import java.awt.Container;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * Klasa koja prikazuje predani graf
 * @author ivorp
 *
 */
public class BarChartDemo extends JFrame{
	BarChart chart;
	String path;

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Konstruktor koji stvara novu instancu klase {@link BarChartDemo}
	 * @param chart Graf koji želite prikazati
	 * @param path Put do datoteke u kojoj su zapisani podaci o grafu
	 */
	public BarChartDemo(BarChart chart, String path) {
		this.path = path;
		this.chart = chart;
		setLocation(20, 50);
		setSize(800, 500);
		setTitle("Graf");
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		
		
		initGUI();
	}
	
	

	/**
	 * Metoda koja dodaje labelu i graf u gui
	 */
	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		cp.add(new JLabel(path, SwingConstants.CENTER), BorderLayout.PAGE_START);
		
		cp.add(new BarChartComponent(chart), BorderLayout.CENTER);
		
	}



	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("pogrešni argumenti");
			return;
		}
		List<XYValue> lista = new ArrayList<>();
		String desX, desY;
		int yMin, yMax, yInc;
		try {
			BufferedReader br = new BufferedReader(
					new InputStreamReader(
					new BufferedInputStream(
					new FileInputStream(args[0])),"UTF-8"));
			desX = br.readLine();
			desY = br.readLine();
			String tockeRed = br.readLine();
			String[] tocke = tockeRed.split(" ");
			for (String tocka : tocke) {
				lista.add(new XYValue(
						Integer.parseInt(tocka.substring(0, tocka.indexOf(","))),  
						Integer.parseInt(tocka.substring(tocka.indexOf(",") + 1))));
			}
			yMin = Integer.parseInt(br.readLine());
			yMax = Integer.parseInt(br.readLine());
			yInc = Integer.parseInt(br.readLine());
			br.close();
			BarChart chart = new BarChart(lista, desX, desY, yMin, yMax, yInc);
			SwingUtilities.invokeLater(() -> {
				JFrame frame = new BarChartDemo(chart, args[0]);
				frame.setVisible(true);
			});
		} catch(IllegalArgumentException e) {
			System.out.println(e.getMessage());
		} catch(FileNotFoundException e) {
			System.out.println("Predana datoteka ne postoji");
		} catch(IOException e) {
			System.out.println("Došlo je do greške, molim pokušajte ponovo");
		} 
		
		
	}

}
