package hr.fer.zemris.java.gui.calc;


import java.awt.Color;
import java.awt.Container;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import hr.fer.zemris.java.gui.calc.buttons.MojGumbBinarneOperacije;
import hr.fer.zemris.java.gui.calc.buttons.MojGumbPotenciranje;
import hr.fer.zemris.java.gui.calc.buttons.MojGumbUnarneOperacije;
import hr.fer.zemris.java.gui.calc.buttons.MojGumbZnamenke;
import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;


/**
 * Klasa koja predstavlja kalkulator
 * @author ivorp
 *
 */
public class Calculator extends JFrame {
	private Stack<Double> stack = new Stack<>(); 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static JLabel label = new JLabel("0");
	private CalcModel model;
	private static List<MojGumbUnarneOperacije> gumbi = new ArrayList<>();
	private static MojGumbPotenciranje pow; 
	
	/**
	 * Konstruktor koji stvara novu instancu razreda Calculator
	 * @param model
	 */
	public Calculator(CalcModel model) {
		this.model = model;
		label.setFont(label.getFont().deriveFont(30f));
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		label.setBackground(Color.YELLOW);
		label.setOpaque(true);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		initGUI();
		pack();


	}
	
	/**
	 * privatna metoda koja služi sa dodavanje svih komponenti u container za prikazivanje
	 */
	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new CalcLayout(3));
		
		JButton rez = new JButton("=");
		rez.addActionListener(a -> {
			try {
				double value = model.getPendingBinaryOperation().applyAsDouble(model.getActiveOperand(), model.getValue());
				model.setPendingBinaryOperation(null);
				model.clearActiveOperand();
				model.setValue(value);
			} catch (IllegalStateException e) {
				System.out.println(e.getMessage());
			}
		});
		
		JButton clear = new JButton("clear");
		clear.addActionListener(a -> {
			model.clear();
		});
		
		JButton reset = new JButton("reset");
		reset.addActionListener(a -> {
			model.clearAll();
		});
		
		JButton swap = new JButton("+/-");
		swap.addActionListener(a -> {
			try {
				model.swapSign();
			}catch (CalculatorInputException e){
				System.out.println(e.getMessage());
			}
		});
		
		JButton dot = new JButton(".");
		dot.addActionListener(a -> {
			try {
				model.insertDecimalPoint();
			}catch (CalculatorInputException e) {
				System.out.println(e.getMessage());
			}
		});
		
		JButton rev = new JButton("1/x");
		rev.addActionListener(a -> {
			model.setValue(1.0 / model.getValue());
		});
		
		JButton push = new JButton("push");
		push.addActionListener(a -> {
			stack.push(Double.valueOf(model.getValue()));
		});
		
		JButton pop = new JButton("pop");
		pop.addActionListener(a -> {
			if (stack.isEmpty())
				System.out.println("Stog je prazan.");
			else
				model.setValue(stack.pop());
		});
		
		pow = new MojGumbPotenciranje("x^n", "x^(1/n)", model, Operators.POW, Operators.ROOT);
		
		MojGumbUnarneOperacije sin = new MojGumbUnarneOperacije("sin", "arcsin", model, Operators.SIN, Operators.ASIN);
		MojGumbUnarneOperacije cos = new MojGumbUnarneOperacije("cos", "arccos", model, Operators.COS, Operators.ACOS);
		MojGumbUnarneOperacije tan = new MojGumbUnarneOperacije("tan", "arctan", model, Operators.TAN, Operators.ATAN);
		MojGumbUnarneOperacije ctg = new MojGumbUnarneOperacije("ctg", "arcctg", model, Operators.CTG, Operators.ACTG);
		MojGumbUnarneOperacije log = new MojGumbUnarneOperacije("log", "10^x", model, Operators.LOG, Operators.ALOG);
		MojGumbUnarneOperacije ln = new MojGumbUnarneOperacije("ln", "e^x", model, Operators.LN, Operators.ALN);
		
		gumbi.add(sin);
		gumbi.add(cos);
		gumbi.add(tan);
		gumbi.add(ctg);
		gumbi.add(log);
		gumbi.add(ln);
		CalcValueListener l = m -> {
			for (MojGumbUnarneOperacije gumb : gumbi) {
				gumb.promijeni();
			}
			pow.promijeni();
		};
		
		JCheckBox cb = new JCheckBox("Inv");
		cb.addActionListener(a -> {
			l.valueChanged(model);
		});
		
		cp.add(label, new RCPosition(1,1));
		cp.add(rez, new RCPosition(1,6));
		cp.add(clear, new RCPosition(1,7));
		cp.add(rev, new RCPosition(2,1));
		cp.add(sin, new RCPosition(2,2));
		cp.add(new MojGumbZnamenke("7", model), new RCPosition(2,3));
		cp.add(new MojGumbZnamenke("8", model), new RCPosition(2,4));
		cp.add(new MojGumbZnamenke("9", model), new RCPosition(2,5));
		cp.add(new MojGumbBinarneOperacije("/", model, Operators.DIV), new RCPosition(2,6));
		cp.add(reset, new RCPosition(2,7));
		cp.add(log, new RCPosition(3,1));
		cp.add(cos, new RCPosition(3,2));
		cp.add(new MojGumbZnamenke("4", model), new RCPosition(3,3));
		cp.add(new MojGumbZnamenke("5", model), new RCPosition(3,4));
		cp.add(new MojGumbZnamenke("6", model), new RCPosition(3,5));
		cp.add(new MojGumbBinarneOperacije("*", model, Operators.MUL), new RCPosition(3,6));
		cp.add(push, new RCPosition(3,7));
		cp.add(ln, new RCPosition(4,1));
		cp.add(tan, new RCPosition(4,2));
		cp.add(new MojGumbZnamenke("1", model), new RCPosition(4,3));
		cp.add(new MojGumbZnamenke("2", model), new RCPosition(4,4));
		cp.add(new MojGumbZnamenke("3", model), new RCPosition(4,5));
		cp.add(new MojGumbBinarneOperacije("-", model, Operators.SUB), new RCPosition(4,6));
		cp.add(pop, new RCPosition(4,7));
		cp.add(pow, new RCPosition(5,1));
		cp.add(ctg, new RCPosition(5,2));
		cp.add(new MojGumbZnamenke("0", model), new RCPosition(5,3));
		cp.add(swap, new RCPosition(5,4));
		cp.add(dot, new RCPosition(5,5));
		cp.add(new MojGumbBinarneOperacije("+", model, Operators.ADD), new RCPosition(5,6));
		cp.add(cb, new RCPosition(5,7));
		
		
	}
	public static void main(String[] args) {
		CalcModel model = new CalcModelImpl();
		CalcValueListener listener = mod -> {
			label.setText(mod.toString());
		};
		model.addCalcValueListener(listener);
		SwingUtilities.invokeLater(()->{
			new Calculator(model).setVisible(true);
			});
		
	}
		
}

